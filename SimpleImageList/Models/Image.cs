﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleImageList.Models
{
    public class Image
    {
        public Guid id { get; set; }

        public String title { get; set; }

        public String path { get; set; }
    }
}
