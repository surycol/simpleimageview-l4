﻿'use strict'

imageListApp.controller('uploadImageController', function ($scope, Upload) {
    var $ctrl = this;

    $ctrl.$onInit = function () {

    }
    $ctrl.ok = function (file) {
        $scope.f = file;

        if (file) {
            file.upload = Upload.upload({
                url: '/api/images/upload',
                data: { title: $scope.picTitle, files: file }
            });

            file.upload.then(function (response) {

                $ctrl.close({ $value: response.data })
                

            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
        }
    }

    $ctrl.cancel = function () {
        $ctrl.dismiss({ $value: 'cancel' });
    }

})

imageListApp.component('uploadImage', {
    templateUrl: 'js/components/uploadImage/uploadImage.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: 'uploadImageController'
})